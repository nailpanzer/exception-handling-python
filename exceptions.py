from typing import Any


class QueueIsEmptyException(Exception):
    pass


class CommandException(Exception):
    pass


class HandleException(Exception):
    def __init__(self, context: Any, exc: Exception) -> None:
        super().__init__(context, exc)
        self.context = context
        self.exc = exc


class CriticalErrorException(Exception):
    def __init__(self, exc: Exception) -> None:
        super().__init__(exc)
        self.exc = exc


class DataIsNoneException(CommandException):
    pass


class LightsArrayIsNoneException(CommandException):
    def __init__(self, data: Any) -> None:
        super().__init__()
        self.data = data


# we can use default IndexError but it won't be catched
# because it is not CommandException
class OutOfRangeException(CommandException):
    pass


class AssertationException(CommandException):
    def __init__(self, received: Any, expected: Any) -> None:
        super().__init__(f"Received: {received}. Expected: {expected}")
        self.received = received
        self.expected = expected
