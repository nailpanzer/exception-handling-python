from abc import ABC, abstractmethod
from typing import Any, Dict, List, Type
from pydantic import BaseModel

from exceptions import (
    AssertationException,
    CommandException,
    CriticalErrorException,
    DataIsNoneException,
    HandleException,
    LightsArrayIsNoneException,
    OutOfRangeException,
    QueueIsEmptyException,
)


#
# Abstractions
#
class Command(ABC):
    @abstractmethod
    def execute(self) -> None:
        pass


class IEventLoop:
    @abstractmethod
    def add_command(self, cmd: Command) -> None:
        pass

    def update(self) -> None:
        pass


class IHandler:
    # the 'handle' method gets an exception to check its arguments.
    # for example, there is a mistake in a word that can be corrected and tried again like this:
    #
    # word = make_correct(exc.word)
    # loop.add_command(Command(word))
    #
    @abstractmethod
    def handle(self, exc: CommandException) -> None:
        pass


class IExceptionHandler(ABC):
    @abstractmethod
    def add_handler(
        self, context_type: Type, exc_type: Type, handler: IHandler
    ) -> None:
        pass

    @abstractmethod
    def handle(self, context: Type, exc: Exception) -> None:
        pass


#
# Let's say we have some kind of program
#
class SomeData(BaseModel):
    lights: List[bool] | None = None


#
# Some commands that control the program and raise CommandExceptions
#
class CmdCreateLights(Command):
    def __init__(self, data: SomeData, count: int, value: bool) -> None:
        self.data = data
        self.count = count
        self.value = value

    def execute(self) -> None:
        if self.data is None:
            raise DataIsNoneException

        self.data.lights = [self.value for _ in range(self.count)]


class CmdSetLights(Command):
    def __init__(self, data: SomeData, value: bool) -> None:
        self.data = data
        self.value = value

    def execute(self) -> None:
        if self.data is None:
            raise DataIsNoneException

        if self.data.lights is None:
            raise LightsArrayIsNoneException(self.data)

        self.data.lights = [self.value for _ in self.data.lights]


class CmdSetILight(Command):
    def __init__(self, data: SomeData, light_index: int, value: bool) -> None:
        self.data = data
        self.light_index = light_index
        self.value = value

    def execute(self) -> None:
        if self.data is None:
            raise DataIsNoneException

        if self.data.lights is None:
            raise LightsArrayIsNoneException(self.data)

        if self.light_index >= len(self.data.lights) or self.light_index < 0:
            raise OutOfRangeException

        self.data.lights[self.light_index] = self.value


class CmdCheckILight(Command):
    def __init__(self, data: SomeData, light_index: int, check_val: bool) -> None:
        self.data = data
        self.light_index = light_index
        self.check_val = check_val

    def execute(self) -> None:
        if self.data is None:
            raise DataIsNoneException

        if self.data.lights is None:
            raise LightsArrayIsNoneException(self.data)

        if self.light_index >= len(self.data.lights) or self.light_index < 0:
            raise OutOfRangeException

        if self.data.lights[self.light_index] != self.check_val:
            raise AssertationException(
                self.data.lights[self.light_index], self.check_val
            )


#
# Commands for exception handling
#
class HandlerEmpty(IHandler):
    def __init__(self, log: List[str], log_str: str) -> None:
        self.log = log
        self.log_str = log_str

    def handle(self, exc: CommandException) -> None:
        self.log.append(self.log_str)


class HandlerCritical(IHandler):
    def __init__(self, log: List[str]) -> None:
        self.log = log

    def handle(self, exc: CommandException) -> None:
        self.log.append(f"ERROR: Critical error {type(exc)}")
        raise CriticalErrorException(exc)


class HandlerArrayIsNone(IHandler):
    def __init__(self, log: List[str], default_array: List[Any]) -> None:
        self.log = log
        self.default_array = default_array

    def handle(self, exc: LightsArrayIsNoneException) -> None:
        self.log.append(f"Array is None. Initiated by default: {self.default_array}")
        exc.data.lights = self.default_array.copy()


class HandlerAssertation(IHandler):
    def __init__(self, log: List[str]) -> None:
        self.log = log

    def handle(self, exc: AssertationException) -> None:
        self.log.append(
            f"ASSERTATION ERROR: Received: {exc.received}. Expected: {exc.expected}"
        )


#
# Exception handling
#
class ExceptionHandler(IExceptionHandler):
    # p, m - are vars for _hash_function
    p = 31
    m = 100

    def __init__(self, table: Dict[int, IHandler] = {}) -> None:
        self.table = table

    def add_handler(
        self, context_type: Type, exc_type: Type, handler: IHandler
    ) -> None:
        hash_code = self._hash_function(context_type, exc_type)

        self.table[hash_code] = handler

    @staticmethod
    def _hash_function(context_type: Type, exc_type: Type) -> int:
        s = str(str(context_type) + "|" + str(exc_type)).lower()

        hash_code = 0
        for i in range(len(s)):
            hash_code += ord(s[i]) * (ExceptionHandler.p) ** i

        return hash_code % ExceptionHandler.m

    def handle(self, context_type: Type, exc: Exception) -> None:
        hash_code = self._hash_function(context_type, type(exc))

        if hash_code not in self.table:
            raise HandleException(context_type, exc)

        self.table[hash_code].handle(exc)


#
# EventLoop
#
class EventLoop(IEventLoop):
    def __init__(
        self, exc_handler: IExceptionHandler, command_queue: List[Command] = []
    ) -> None:
        self.cmd_queue = command_queue
        self.exc_handler = exc_handler

    def add_command(self, cmd: Command) -> None:
        self.cmd_queue.append(cmd)

    def update(self) -> None:
        if len(self.cmd_queue) == 0:
            raise QueueIsEmptyException

        cmd = self.cmd_queue.pop(0)

        try:
            print(f"Command {type(cmd)} executing...", end=" ")
            cmd.execute()
            print("Done")
        except CommandException as exc:
            print(f"Error {type(exc)}")
            self.exc_handler.handle(type(cmd), exc)


#
# Usage example
#
log: List[str] = []

exc_handler = ExceptionHandler()

command_queue: List[Command] = []

loop = EventLoop(exc_handler, command_queue=command_queue)


# if data is none
exc_handler.add_handler(CmdCreateLights, DataIsNoneException, HandlerCritical(log))
exc_handler.add_handler(CmdSetLights, DataIsNoneException, HandlerCritical(log))
exc_handler.add_handler(CmdSetILight, DataIsNoneException, HandlerCritical(log))
exc_handler.add_handler(CmdCheckILight, DataIsNoneException, HandlerCritical(log))

# if array is none than create array
exc_handler.add_handler(
    CmdCreateLights, LightsArrayIsNoneException, HandlerArrayIsNone(log, [False, False])
)
exc_handler.add_handler(
    CmdSetLights, LightsArrayIsNoneException, HandlerArrayIsNone(log, [False, False])
)
exc_handler.add_handler(
    CmdSetILight, LightsArrayIsNoneException, HandlerArrayIsNone(log, [False, False])
)
exc_handler.add_handler(
    CmdCheckILight, LightsArrayIsNoneException, HandlerArrayIsNone(log, [False, False])
)

# if out of range than just add some text in log
exc_handler.add_handler(
    CmdCreateLights, OutOfRangeException, HandlerEmpty(log, "Out of range")
)
exc_handler.add_handler(
    CmdSetLights, OutOfRangeException, HandlerEmpty(log, "Out of range")
)
exc_handler.add_handler(
    CmdSetILight, OutOfRangeException, HandlerEmpty(log, "Out of range")
)
exc_handler.add_handler(
    CmdCheckILight, OutOfRangeException, HandlerEmpty(log, "Out of range")
)

# if assertation error
exc_handler.add_handler(CmdCreateLights, AssertationException, HandlerAssertation(log))
exc_handler.add_handler(CmdSetLights, AssertationException, HandlerAssertation(log))
exc_handler.add_handler(CmdSetILight, AssertationException, HandlerAssertation(log))
exc_handler.add_handler(CmdCheckILight, AssertationException, HandlerAssertation(log))


program_data = SomeData()

loop.add_command(CmdSetLights(program_data, False))  # LightsArrayIsNoneException
# now there will be no exception, because Handler will create a array
loop.add_command(CmdSetLights(program_data, False))

loop.add_command(CmdCreateLights(program_data, 10, False))
loop.add_command(CmdCreateLights(None, 10, False))  # DataIsNoneException
loop.add_command(CmdSetLights(None, False))  # DataIsNoneException
loop.add_command(CmdSetLights(SomeData(), True))  # LightsArrayIsNoneException
loop.add_command(CmdSetLights(program_data, False))
loop.add_command(CmdSetILight(program_data, 8, True))
loop.add_command(CmdSetILight(program_data, 200, True))  # OutOfRangeException
loop.add_command(CmdSetILight(SomeData(), 8, True))  # LightsArrayIsNoneException
loop.add_command(CmdSetILight(None, 4, True))  # DataIsNoneException
loop.add_command(CmdCheckILight(program_data, 8, True))
loop.add_command(CmdCheckILight(None, 8, True))  # DataIsNoneException
loop.add_command(CmdCheckILight(SomeData(), 8, True))  # LightsArrayIsNoneException
loop.add_command(CmdCheckILight(program_data, 200, True))  # OutOfRangeException
loop.add_command(CmdCheckILight(program_data, 8, False))  # AssertationException


while True:
    try:
        loop.update()
    except QueueIsEmptyException as exc:
        break
    except CriticalErrorException as exc:
        print(f"Critical error {type(exc.exc)}!")


# Some log
print("\n")
print(f"Log size: {len(log)}")
for s in log:
    print(s)

# Output:
"""
    Log size: 11
    Array is None. Initiated by default: [False, False]
    ERROR: Critical error <class 'exceptions.DataIsNoneException'>
    ERROR: Critical error <class 'exceptions.DataIsNoneException'>
    Array is None. Initiated by default: [False, False]
    Out of range
    Array is None. Initiated by default: [False, False]
    ERROR: Critical error <class 'exceptions.DataIsNoneException'>
    ERROR: Critical error <class 'exceptions.DataIsNoneException'>
    Array is None. Initiated by default: [False, False]
    Out of range
    ASSERTATION ERROR: Received: True. Expected: False
"""
